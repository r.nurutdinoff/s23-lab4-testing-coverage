package com.hw.db.dao;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

import java.util.Optional;

class UserDAOTest {
    @Test
    void userTest1() {
        User user = new User("keku", "email", null, "test");
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void userTest2() {
        User user = new User("keku", null, null, "test");
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void userTest3() {
        User user = new User("keku", "email", "kukech", "test");
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void userTest4() {
        User user = new User("keku", "email", "kukech", null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void userTest5() {
        User user = new User("keku", null, "kukech", null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void userTest6() {
        User user = new User("keku", null, "kukech", "test");
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void userTest7() {
        User user = new User("keku", "email", null, null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"), Optional.ofNullable(Mockito.any()));
    }
}
