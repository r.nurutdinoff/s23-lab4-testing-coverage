package com.hw.db.dao;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import java.sql.Timestamp;
import java.util.stream.Stream;
import static org.mockito.Mockito.*;

class PostDAOTest {
        private static JdbcTemplate mockJdbc;
        private static PostDAO postDao;
        private static Post post;
        private static final Integer POST_ID = 5;
        private static final String AUTHOR = "keku";
        private static final String MESSAGE = "kukech";
        private static final Timestamp CREATED = new Timestamp(System.currentTimeMillis());
    
        @BeforeEach
        void init() {
            mockJdbc = mock(JdbcTemplate.class);
            postDao = new PostDAO(mockJdbc);
    
            post = new Post();
            post.setId(POST_ID);
            post.setAuthor("");
            post.setMessage("");
            post.setCreated(new Timestamp(0));
        }
    
        private static Stream<Arguments> ioParams() {
            return Stream.of(
                    Arguments.of(POST_ID, AUTHOR, MESSAGE, CREATED, "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                    Arguments.of(POST_ID, null, MESSAGE, null, "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                    Arguments.of(POST_ID, AUTHOR, null, null, "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                    Arguments.of(POST_ID, null, MESSAGE, CREATED, "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                    Arguments.of(POST_ID, null, null, CREATED, "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                    Arguments.of(POST_ID, AUTHOR, null, CREATED, "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                    Arguments.of(POST_ID, AUTHOR, MESSAGE, null, "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;")
            );
        }
    
        @ParameterizedTest
        @MethodSource("ioParams")
        void basisPathTestSetPost(Integer id, String author, String message, Timestamp created, String expected) {
            Mockito.when(mockJdbc.queryForObject(
                    Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                    Mockito.any(PostDAO.PostMapper.class),
                    Mockito.eq(POST_ID))
            ).thenReturn(post);
    
            Post post = new Post();
            post.setId(id);
            post.setAuthor(author);
            post.setMessage(message);
            post.setCreated(created);
    
            PostDAO.setPost(POST_ID, post);
    
            verify(mockJdbc).update(Mockito.eq(expected), (Object[]) Mockito.any());
        }
    

        @Test
        void everythingIsNull() {
            Post post = new Post();
            PostDAO.setPost(POST_ID, post);
            verify(mockJdbc, never()).update(Mockito.any(), (Object[]) Mockito.any());
        }
}
