package com.hw.db.dao;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class ThreadDAOTest {
    JdbcTemplate jdbcMock = Mockito.mock(JdbcTemplate.class);

    ThreadDAO threadDAO = new ThreadDAO(jdbcMock);

    private final String query1 = "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;";
    private final String query2 = "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;";

    @Test
    void threadTest1() {
        ThreadDAO.treeSort(100, 100, 20, true);

        Mockito.verify(jdbcMock).query(
                Mockito.eq(query1),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any());
    }

    @Test
    void threadTest2() {
        ThreadDAO.treeSort(100, 100, 20, false);

        Mockito.verify(jdbcMock).query(
                Mockito.eq(query2),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any());
    }
}
