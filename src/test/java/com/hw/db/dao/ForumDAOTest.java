package com.hw.db.dao;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ForumDAOTest {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forumDao;

        @BeforeEach
        void init() {
                this.forumDao = new ForumDAO(mockJdbc);
        }

        @Test
        void forumTest1() {
                ForumDAO.UserList("stub", null, "12.12.2012", true);
                verify(mockJdbc).query(
                                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                                                "WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
                                Mockito.any(Object[].class),
                                Mockito.any(UserDAO.UserMapper.class));
        }

        @Test
        void forumTest2() {
                ForumDAO.UserList("stub", 5, null, true);
                verify(mockJdbc).query(
                                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                                                "WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
                                Mockito.any(Object[].class),
                                Mockito.any(UserDAO.UserMapper.class));
        }

        @Test
        void forumTest3() {
                ForumDAO.UserList("stub", null, null, false);
                verify(mockJdbc).query(
                                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                                                "WHERE forum = (?)::citext ORDER BY nickname;"),
                                Mockito.any(Object[].class),
                                Mockito.any(UserDAO.UserMapper.class));
        }

        @Test
        void forumTest4() {
                ForumDAO.UserList("stub", 5, "12.12.2012", true);
                verify(mockJdbc).query(
                                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                                                "WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                                Mockito.any(Object[].class),
                                Mockito.any(UserDAO.UserMapper.class));
        }

        @Test
        void forumTest5() {
                ForumDAO.UserList("stub", 5, null, false);
                verify(mockJdbc).query(
                                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                                                "WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
                                Mockito.any(Object[].class),
                                Mockito.any(UserDAO.UserMapper.class));
        }

        @Test
        void forumTest6() {
                ForumDAO.UserList("stub", 5, "12.12.2012", false);
                verify(mockJdbc).query(
                                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                                                "WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
                                Mockito.any(Object[].class),
                                Mockito.any(UserDAO.UserMapper.class));
        }

        @Test
        void forumTest8() {
                ForumDAO.UserList("stub", null, null, true);
                verify(mockJdbc).query(
                                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                                                "WHERE forum = (?)::citext ORDER BY nickname desc;"),
                                Mockito.any(Object[].class),
                                Mockito.any(UserDAO.UserMapper.class));
        }

        @Test
        void forumTest7() {
                ForumDAO.UserList("stub", null, "12.12.2012", false);
                verify(mockJdbc).query(
                                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users " +
                                                "WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                                Mockito.any(Object[].class),
                                Mockito.any(UserDAO.UserMapper.class));
        }

}